# StartFromTray

This amazing app was written for easy running from tray the most used programs, especially for don’t have a link in Start, and also for any documents, that you have to open. In addition, it may be used for running your scripts.

Эта замечательная программа написана для облегчения запуска из трея наиболее часто используемых программ, особенно для тех, для которых нет ярлыка, а также для любых других документов, которые приходится когда-либо открывать. Также она может быть полезна для запуска каких-либо скриптов.
